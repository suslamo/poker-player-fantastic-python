from utils import (
    getIntRank,
    buildCards,
    no_bets_yet,
    probability_against_random_hand
)
from flop import Flop
from strategy import Strategy
import logging


class River(Flop):
    def action(self):
        board_rank = getIntRank(buildCards(self.board[3:]), buildCards(self.board[0:3]))
        hand = buildCards(self.hole_cards)
        board = buildCards(self.board)
        rank = getIntRank(board, hand)

        if board_rank == rank:
            if rank <= 5:
                return self.call_amount
            return 0

        return super(River, self).action()
