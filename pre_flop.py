from utils import (
    current_all_in_number,
    players_in_allin,
    isPicture,
    is_connector,
    is_same_suite,
    is_all_in_pair,
    no_bets_yet
)
from strategy import Strategy
import random


class PreFlop(Strategy):

    def action(self):
        allin_players = players_in_allin(self.players)
        allin_names = map(lambda p: p['name'], allin_players)
        if self.active_players > 2:
            if ('Double Belly Busters' in allin_names) or ('Raccoon' in allin_names):
                if is_all_in_pair(self.hole_cards) and self.hole_cards[0]['rank'].upper() in ('Q', 'K', 'A'):
                    return self.stack
                return 0

        if current_all_in_number(self.players) <= 2:
            if is_all_in_pair(self.hole_cards):
                return self.stack
            if isPicture(self.hole_cards[0]) and isPicture(self.hole_cards[1]):
                return self.random_raise

        bet_on_table = self.value_of_single_bet_on_table()
        if bet_on_table > 0 and bet_on_table <= 0.10:
            if isPicture(self.hole_cards[0]) or isPicture(self.hole_cards[1]):
                return self.min_raise if random.random() < 0.5 else self.call_amount

        if self.should_be_agressive:
            if is_connector(self.hole_cards) and is_same_suite(self.hole_cards):
                return self.min_raise
            if no_bets_yet(self.players):
                return self.min_raise

        if self.current_bet_size <= 0.02:
            return self.call_amount

        # if short stack
        if self.stack <= 200 or self.should_be_agressive:
            return self.stack

        return 0
