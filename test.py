import unittest
import player

from utils import (
    getIntRank,
    buildCards,
    no_bets_yet,
    probability_against_random_hand
)

class unitTest_add_numbers(unittest.TestCase):
  """testing add_numbers"""
  
  def test_add_numbers(self):
    self.assertEquals(1,1,"adding 4 and 5")

class flop_tests(unittest.TestCase):
  """Tests for strategy on flop"""

  def test(self):
    hand = buildCards([ { "rank": "A", "suit": "hearts" }, { "rank": "A", "suit": "spades" } ])
    board = buildCards( [ { "rank": "K", "suit": "hearts" }, { "rank": "2", "suit": "spades" }, { "rank": "7", "suit": "spades" } ])
    p = probability_against_random_hand(board, hand)
    self.assertGreater(p, 0.5)

if __name__ == '__main__':
  unittest.main()


