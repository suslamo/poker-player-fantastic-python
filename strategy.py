import random


class Strategy(object):

    def __init__(self, game_state):
        self.data = game_state

    @property
    def players(self):
        return self.data["players"]

    @property
    def in_action(self):
        return self.data["in_action"]

    @property
    def player(self):
        return self.players[self.in_action]

    @property
    def hole_cards(self):
        return self.player['hole_cards']

    @property
    def stack(self):
        return self.player['stack']

    @property
    def bet(self):
        return self.player['bet']

    @property
    def current_buy_in(self):
        return self.data['current_buy_in']

    @property
    def call_amount(self):
        return self.current_buy_in - self.bet

    @property
    def min_raise(self):
        return self.call_amount + self.data['minimum_raise']

    @property
    def bet_index(self):
        return self.data['bet_index']

    @property
    def board(self):
        return self.data['community_cards']

    @property
    def random_raise(self):
        return self.stack if random.random() < 0.1 else self.min_raise

    @property
    def current_max_bet(self):
        return max(player['bet'] for player in self.players)

    @property
    def current_bet_size(self):
        return self.current_max_bet / float(self.stack)

    @property
    def big_blinds(self):
        return self.stack / (self.data['small_blind'] * 2)

    @property
    def active_players(self):
        return len(self.players) - len([player for player in self.players if player['status'] == 'out'])

    @property
    def should_be_agressive(self):
        return self.active_players <= 3 or self.big_blinds <= 5

    @property
    def pot(self):
        return self.data['pot']

    def value_of_single_bet_on_table(self):
        bets = [player['bet'] for player in self.players if player['bet']]
        if len(bets) != 1:
            return 0
        return float(bets[0]) / float(self.stack)
