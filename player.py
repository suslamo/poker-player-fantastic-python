from pre_flop import PreFlop
from flop import Flop
#from river import River
from utils import is_preflop, is_river


class Player:
    VERSION = "3.1.3.3.7"

    def betRequest(self, game_state):
        s = self.createStrategy(game_state)
        return s.action()

    def createStrategy(self, game_state):
        if is_preflop(game_state):
            return PreFlop(game_state)
        #elif is_river(game_state):
        #    return River(game_state)
        return Flop(game_state)

    def showdown(self, game_state):
        pass
