from utils import (
    getIntRank,
    buildCards,
    no_bets_yet,
    probability_against_random_hand
)
from strategy import Strategy
import logging


class Flop(Strategy):
    def bet_value(self, bet):
        if(self.pot + bet > 0):
            return bet / float(self.pot + bet)
        return 2 # anything > 1 :)

    def action(self):
        hand = buildCards(self.hole_cards)
        board = buildCards(self.board)
        rank = getIntRank(board, hand)

        rank_to_raise = 8 if self.should_be_agressive else 7 # pair or two pairs
        rank_to_call = 8 # pair

        if rank <= rank_to_raise: # two pair of better
            return self.random_raise
        elif no_bets_yet(self.players):
            return self.min_raise
        elif rank <= rank_to_call:
            return self.call_amount
        else:
            try:
                p = probability_against_random_hand(board, hand)
                if self.bet_value(self.min_raise) + 0.05 < p:
                    return self.min_raise
                if self.bet_value(self.call_amount) + 0.05 < p:
                    return self.call_amount
            except:
                logging.warning("probability failed", exc_info = True)
                return 0

        return 0
