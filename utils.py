from deuces import Card, Evaluator, Deck

pictures = ['J', 'Q', 'K', 'A']
ten_or_higher_cards = ['10', 'J', 'Q', 'K', 'A']
cards = [str(num) for num in range(2, 11)] + pictures


def isPicture(card):
    return card['rank'].upper() in ['Q', 'K', 'A']


def is_all_in_pair(pair):
    return pair[1]['rank'] == pair[0]['rank'] and pair[0]['rank'].upper() in ten_or_higher_cards


def is_connector(pair):
    return abs(
        cards.index(pair[0]['rank']) - cards.index(pair[1]['rank'])) == 1


def is_same_suite(pair):
    return pair[0]['suit'] == pair[1]['suit']


def is_preflop(game_state):
    cc = game_state['community_cards']
    return (not cc) or (len(cc) == 0)


def is_afterflop(game_state):
    cc = game_state['community_cards']
    return cc and (len(cc) > 0)


def is_flop(game_state):
    cc = game_state['community_cards']
    return cc and (len(cc) == 3)


def is_turn(game_state):
    cc = game_state['community_cards']
    return cc and (len(cc) == 4)


def is_river(game_state):
    cc = game_state['community_cards']
    return cc and (len(cc) == 5)


def no_bets_yet(players):
    return not any(player['bet'] for player in players)


def current_all_in_number(players):
    return len(players_in_allin(players))

def players_in_allin(players):
    result = []
    for player in players:
        if player['bet'] and not player['stack']:
            result.append(player)
    return result

# less num is better
def getIntRank(board, hand):
    e = Evaluator()
    return e.get_rank_class(e.evaluate(board, hand))

#        1 : "Straight Flush",
#        2 : "Four of a Kind",
#        3 : "Full House",
#        4 : "Flush",
#        5 : "Straight",
#        6 : "Three of a Kind",
#        7 : "Two Pair",
#        8 : "Pair",
#        9 : "High Card"


def getStrRank(board, hand):
    e = Evaluator()
    return e.class_to_string(e.get_rank_class(e.evaluate(board, hand)))


def parseCard(rank, suit):
    # where As = Ace of Spades \m/
    return rank + suit[0]


def buildCards(arr):
    result = []
    for card in arr:
        rank = "T" if card["rank"] == "10" else card["rank"]
        result.append(Card.new(parseCard(rank, card["suit"])))
    return result

def hand_is_better(e, board, hand1, hand2):
    return e.evaluate(board, hand1) < e.evaluate(board, hand2)

def probability_against_random_hand(board, hand):
    cnt = 1000
    e = Evaluator()
    random_opponents = [Deck().draw(2) for i in xrange(cnt)]
    beaten = len( [True for r in random_opponents if hand_is_better(e, board, hand, r)] )
    return float(beaten) / float(cnt)
